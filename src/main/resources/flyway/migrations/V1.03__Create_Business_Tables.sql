
CREATE TABLE BUSINESS
(
    business_id INT NOT NULL AUTO_INCREMENT,
    bunsiness_name VARCHAR(100) NOT NULL,
    description VARCHAR(255),
    schedular_active BOOLEAN NOT NULL DEFAULT false,
	created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    modified_date TIMESTAMP NULL,
    created_by INTEGER NOT NULL,
    modified_by INTEGER ,
    PRIMARY KEY (business_id) ,
    FOREIGN KEY (created_by) REFERENCES USERS (user_id),
    FOREIGN KEY (modified_by) REFERENCES USERS (user_id)   
);

CREATE TABLE BUSINESS_CATEGORY
(
    business_id INTEGER NOT NULL,
    category_id INTEGER NOT NULL,
	created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    modified_date TIMESTAMP NULL,
    created_by INTEGER NOT NULL,
    modified_by INTEGER,
	PRIMARY KEY(business_id, category_id) ,
    FOREIGN KEY (business_id) REFERENCES BUSINESS (business_id),
    FOREIGN KEY (category_id) REFERENCES CATEGORY (category_id),
    FOREIGN KEY (category_id) REFERENCES CATEGORY (category_id),
	FOREIGN KEY (created_by) REFERENCES USERS (user_id),
    FOREIGN KEY (modified_by) REFERENCES USERS (user_id)
);

CREATE TABLE COUNTRIES
(
    country_id INT NOT NULL AUTO_INCREMENT,
    country_name VARCHAR(50)  NOT NULL,
    country_code VARCHAR(10)  NOT NULL,
	created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    modified_date TIMESTAMP NULL,
    PRIMARY KEY(country_id),
    UNIQUE (country_code)
);

CREATE TABLE STATES
(
    state_id INT NOT NULL AUTO_INCREMENT,
    state_name VARCHAR(50) NOT NULL,
    country_id INTEGER NOT NULL,
 	created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    modified_date TIMESTAMP NULL,
    PRIMARY KEY(state_id),
    UNIQUE (state_id, country_id),
    FOREIGN KEY(country_id) REFERENCES COUNTRIES (country_id) 
);

CREATE TABLE CITIES
(
    city_id INT NOT NULL AUTO_INCREMENT,
    state_id INTEGER NOT NULL,
    city_name VARCHAR(50) NOT NULL,
	created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    modified_date TIMESTAMP NULL,
    PRIMARY KEY(city_id),
    FOREIGN KEY (state_id) REFERENCES STATES (state_id)

);

CREATE TABLE ADDRESS
(
    address_id INT NOT NULL AUTO_INCREMENT,
    city_id INTEGER NOT NULL,
    zip_code INTEGER NOT NULL,
    created_by INTEGER NOT NULL,
    modified_by INTEGER,
    address_line_1 VARCHAR(60) NOT NULL,
    address_line_2 VARCHAR(60) ,
    address_line_3 VARCHAR(60) ,
 	created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    modified_date TIMESTAMP NULL,
    state_id INTEGER NOT NULL,
	PRIMARY KEY(address_id),
    FOREIGN KEY (city_id) REFERENCES CITIES (city_id),
  	FOREIGN KEY (created_by) REFERENCES USERS (user_id),
    FOREIGN KEY (modified_by) REFERENCES USERS (user_id)
);

CREATE TABLE ADDRESS_TYPE
(
    address_type_id INT NOT NULL AUTO_INCREMENT,
    address_type VARCHAR(30) NOT NULL,
 	created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    modified_date TIMESTAMP NULL,
    PRIMARY KEY(address_type_id)
);

CREATE TABLE LINKED_BUSINESS
(
    linked_id INT NOT NULL AUTO_INCREMENT,
    primary_business_id INTEGER NOT NULL,
    linked_business_id INTEGER NOT NULL,
    created_by INTEGER NOT NULL,
    modified_by INTEGER,
 	created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    modified_date TIMESTAMP NULL,
    PRIMARY KEY(linked_id),
 	FOREIGN KEY (created_by) REFERENCES USERS (user_id),
    FOREIGN KEY (modified_by) REFERENCES USERS (user_id),
	FOREIGN KEY (linked_business_id) REFERENCES BUSINESS (business_id),
    FOREIGN KEY (primary_business_id) REFERENCES BUSINESS (business_id)
);

CREATE TABLE BUSINESS_ADDRESS
(
    business_id INT NOT NULL AUTO_INCREMENT,
    address_id INTEGER NOT NULL,
    address_type_id INTEGER NOT NULL,
    created_by INTEGER NOT NULL,
    modified_by INTEGER,
    created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    modified_date TIMESTAMP NULL,
	PRIMARY KEY(business_id, address_id, address_type_id),
 	FOREIGN KEY (created_by) REFERENCES USERS (user_id),
    FOREIGN KEY (modified_by) REFERENCES USERS (user_id),
	FOREIGN KEY (address_id) REFERENCES ADDRESS (address_id),
    FOREIGN KEY (address_type_id) REFERENCES ADDRESS_TYPE (address_type_id),
    FOREIGN KEY (business_id) REFERENCES BUSINESS (business_id)
);

CREATE TABLE PREFERENCES
(
    preference_id INT NOT NULL AUTO_INCREMENT,
    business_id INTEGER NOT NULL,
    preference_text VARCHAR(255),
    created_by INTEGER NOT NULL,
    modified_by INTEGER,
	created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    modified_date TIMESTAMP NULL,
	PRIMARY KEY(preference_id),
 	FOREIGN KEY (created_by) REFERENCES USERS (user_id),
    FOREIGN KEY (modified_by) REFERENCES USERS (user_id),
    FOREIGN KEY (business_id) REFERENCES BUSINESS (business_id)
);

CREATE TABLE PHOTOS
(
    photo_id INT NOT NULL AUTO_INCREMENT,
    photo_location VARCHAR(100) NOT NULL,
    profile_pic BOOLEAN NOT NULL,
    business_id INTEGER NOT NULL,
    created_by INTEGER NOT NULL,
    modified_by INTEGER,
	created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    modified_date TIMESTAMP NULL,
	PRIMARY KEY(photo_id),
 	FOREIGN KEY (created_by) REFERENCES USERS (user_id),
    FOREIGN KEY (modified_by) REFERENCES USERS (user_id),
    FOREIGN KEY (business_id) REFERENCES BUSINESS (business_id)

);
